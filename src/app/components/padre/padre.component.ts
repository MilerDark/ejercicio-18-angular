import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PadreComponent implements OnInit {

  Objeto1 = {
    nombre: "Wilstermann",
    copas: "125",
    nacionalidad:"Boliviana"
  };

  Objeto2 = {
    pelicula: "Marvel",
    heroe: "Thor",
    tipo:"accion"
  }
  
  constructor() { }

  ngOnInit(): void {
  }
}
